var CONSTANTS = require('./constants');

module.exports = function () {
    'use strict';

    var db;
    var path = require('path');
    var http = require('http');
    var logger = require('morgan');
    var express = require('express');
    var mongoose = require('mongoose');
    var bodyParser = require('body-parser');
    var session = require('express-session');
    var consolidate = require('consolidate');
    var MongoStore = require('connect-mongo')(session);

    require('./helpers/expansionArray')();

    process.env.NODE_ENV = process.env.NODE_ENV || 'development';

    if (process.env.NODE_ENV === 'production') {
        console.log('-----Server start success in Production version--------');
        require('./config/production');
    } else if (process.env.NODE_ENV === 'development') {
        console.log('-----Server start success in Development version--------');
        require('./config/development');
    } else {
        console.log('-----Server start success in TEST version--------');
        require('./config/test');
    }

    db = mongoose.createConnection(process.env.DB_HOST, process.env.DB_NAME, process.env.DB_PORT);

    require('./models/index')(db);

    db.on('error', console.error.bind(console, 'connection error:'));

    db.once('open', function callback() {
        var server;
        var app = express();
        var allowCrossDomain = require('./helpers/allowCrossDomain');

        app.set('db', db);
        app.set('port', process.env.PORT || 9538);

        // app.engine('html', consolidate.ejs);
        // app.set('views', __dirname + '/public/templates/customTemplates');
        // app.set('view engine', 'html');

        app.use(express.static(path.join(__dirname, 'public')));
        app.use(logger('dev'));
        app.use(bodyParser.json({strict: false, limit: '50mb'}));
        app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
        app.use(session({
            secret           : process.env.SESSION_SECRET,
            resave           : true,
            saveUninitialized: true,

            store: new MongoStore({
                mongooseConnection: db,
                autoReconnect     : true,
                ssl               : false,
                ttl               : process.env.COOKIE_MAX_AGE
            }),

            cookie: {
                maxAge : process.env.COOKIE_MAX_AGE,
                expires: new Date(new Date() + process.env.COOKIE_MAX_AGE)
            }
        }));
        app.use(allowCrossDomain);

        /*var User = db.model(CONSTANTS.MODELS.USER);

         // ==== create Super Admin =======
         User.findOne({role: CONSTANTS.ROLES.SUPER_ADMIN}, function (err, adminModel) {
         var user;

         if (err) {
         return console.warn(err);
         }

         if (!adminModel) {
         user = new User({
         fullName: 'Super Admin',
         email   : CONSTANTS.ADMIN_EMAIL,
         password: CONSTANTS.ADMIN_PASSWORD
         });

         user.save(function (err) {
         if (err) {
         return console.warn(err);
         }
         });
         }
         });
         // ================================
         */

        require('./routes')(app);

        console.log('Connection to ' + process.env.DB_NAME + ' is success');

        server = http.createServer(app);

        server.listen(app.get('port'), function () {
            console.log('Express server listening on port ' + app.get('port'));
            console.log('WEB_HOST: ' + process.env.WEB_HOST);
            console.log('DATABASE: ' + process.env.DB_NAME);
        });
    });

};