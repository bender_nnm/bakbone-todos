define(['Backbone', 'models/todo/model'], function (Backbone, TodoModel) {
    'use strict';

    return Backbone.Collection.extend({
        model: TodoModel,
        url  : 'todos'
    });

});
