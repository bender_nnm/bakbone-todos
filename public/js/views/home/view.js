define([
    'Underscore',
    'jQuery',
    'Backbone',
    'views/todos/view',
    'text!templates/home.html'
], function (_, $, Backbone, TodosView, template) {
    'use strict';

    return Backbone.View.extend({
        el: '#wrapper',

        initialize: function () {
            this.render().renderTodos();
        },

        renderTodos: function () {
            this.todosView = new TodosView();
        },

        render: function () {
            this.$el.html(template);

            return this;
        }
    });

});
