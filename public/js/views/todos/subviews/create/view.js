define([
    'Underscore',
    'jQuery',
    'Backbone',
    'text!templates/create.html',
    'constants/index'
], function (_, $, Backbone, template, CONSTANTS) {
    'use strict';

    function validateRating(rating) {
        rating = parseInt(rating, 10);

        if (!rating || rating < 0 || rating > 5) {
            return null;
        }

        return rating;
    }

    return Backbone.View.extend({

        el: '#todos-subview',

        template: _.template(template),

        events: {
            'click #createTodo': 'sendAction'
        },

        initialize: function () {
            this.render();
        },

        sendAction: _.debounce(function () {
            var data = {};
            var title = this.$title.val();
            var rating = validateRating(this.$rating.val());

            if (!title) {
                return;
            }

            data.title = title;

            if (rating) {
                data.rating = rating;
            }

            this.trigger(CONSTANTS.EVENTS.ACTION, CONSTANTS.ACTION_TYPES.CREATE, data);
        }, 1000, true),

        render: function () {
            this.$el.html(this.template);

            this.$title = this.$el.find('#title-field');
            this.$rating = this.$el.find('#rating-field');

            return this;
        }
    });


});
