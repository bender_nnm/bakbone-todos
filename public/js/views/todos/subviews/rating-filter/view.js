define([
    'Underscore',
    'jQuery',
    'Backbone',
    'text!templates/rating-filter.html',
    'constants/index'
], function (_, $, Backbone, template, CONSTANTS) {
    'use strict';

    return Backbone.View.extend({

        el: '#todos-subview',

        template: _.template(template),

        events: {
            'click input:radio'   : 'sendSortAction',
            'change .range-number': 'sendRangeAction'
        },

        initialize: function () {
            this.render();
        },

        sendSortAction: _.debounce(function (e) {
            var $target = $(e.target);
            var filterSortType = $target.val();

            this.trigger(CONSTANTS.EVENTS.ACTION, CONSTANTS.ACTION_TYPES.FILTER_SORT, filterSortType);
        }, 300),

        sendRangeAction: _.debounce(function () {
            var range;
            var lte = this.$lte.val();
            var gte = this.$gte.val();

            if (gte > lte) {
                lte = 1;
                gte = 5;

                this.$lte.val(lte);
                this.$gte.val(gte);
            }

            range = {
                lte: lte,
                gte: gte
            };

            this.trigger(CONSTANTS.EVENTS.ACTION, CONSTANTS.ACTION_TYPES.FILTER_RANGE, range);
        }, 300),

        render: function () {
            this.$el.html(this.template);

            this.$lte = this.$el.find('#lte');
            this.$gte = this.$el.find('#gte');

            return this;
        }
    });


});
