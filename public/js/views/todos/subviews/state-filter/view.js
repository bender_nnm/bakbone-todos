define([
    'Underscore',
    'jQuery',
    'Backbone',
    'text!templates/state-filter.html',
    'constants/index'
], function (_, $, Backbone, template, CONSTANTS) {
    'use strict';

    return Backbone.View.extend({

        el: '#todos-subview',

        template: _.template(template),

        events: {
            'click input:radio': 'sendAction'
        },

        initialize: function () {
            this.render();
        },

        sendAction: _.debounce(function (e) {
            var $target = $(e.target);
            var filterType = $target.val();

            this.trigger(CONSTANTS.EVENTS.ACTION, CONSTANTS.ACTION_TYPES.FILTER_STATE, filterType);
        }, 300),

        render: function () {
            this.$el.html(this.template);

            return this;
        }
    });

});
