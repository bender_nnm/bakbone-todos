define([
    'Underscore', 'jQuery', 'Backbone',
    'models/todo/model',
    'collections/todo/collection',
    'text!templates/todos.html', 'text!templates/todo-list.html', 'text!templates/todo.html',
    'text!templates/control-buttons-block.html', 'text!templates/confirm-buttons-block.html',
    'text!templates/info-block-input.html', 'text!templates/info-block-output.html',
    'constants/index'
], function (_, $, Backbone,
             TodoModel,
             TodoCollection,
             todosTemplate, todoListTemplate, todoTemplate,
             ctrlBtnsTemplate, confirmBtnsTemplate,
             infoBlockInputTemplate, infoBlockOutputTemplate,
             CONSTANTS) {
    'use strict';

    return Backbone.View.extend({
        el: '#todos',

        todoTemplate           : _.template(todoTemplate),
        todosTemplate          : _.template(todosTemplate),
        todoListTemplate       : _.template(todoListTemplate),
        infoBlockInputTemplate : _.template(infoBlockInputTemplate),
        infoBlockOutputTemplate: _.template(infoBlockOutputTemplate),

        events: {
            'click .todo-item-btn.edit'        : 'onEdit',
            'click .todo-item-btn.delete'      : 'onDelete',
            'click .todo-item-btn.todo-cancel' : 'onCancel',
            'click .todo-item-btn.todo-confirm': 'onConfirm',
            'click #todos-ctrl-create'         : 'onCreateTodo',
            'click #todos-ctrl-clear-filter'   : 'onClearFilter',
            'click #todos-ctrl-state-filter'   : 'onStateFilter',
            'click .output.state'              : 'onChangeState',
            'click #todos-ctrl-rating-filter'  : 'onRatingFilter'
        },

        initialize: function () {
            this.filter = {};
            this.subView = null;
            this.confirmEvent = {};
            this.collection = new TodoCollection();

            _.extend(this.confirmEvent, Backbone.Events);

            this.collection.on('error', function (col, res, opts) {
                console.log('ERR!!!');
            });

            this.render().fetchCollection();
        },

        fetchCollection: function (opts) {
            var self = this;
            var query = {};

            opts || (opts = {});

            if (opts.query) {
                query = opts.query;
            }

            if (this.filter) {
                query.filter = this.filter;
            }

            this.confirmEvent.off();
            this.confirmEvent.stopListening();

            this.collection.fetch({
                data   : query,
                success: function () {
                    self.renderList();
                }
            });
        },

        getItemId: function ($target) {
            return $target.closest('.todo-item').attr('data-id');
        },

        onDelete: function (e) {
            var eventName = 'confirm:';
            var $target = $(e.currentTarget);
            var $todoItem = $target.closest('.todo-item');
            var $btnsBlock = $target.closest('.btns-block');
            var id = $todoItem.attr('data-id');
            var model = this.collection.get(id);

            if (!model) {
                return;
            }

            $btnsBlock.html(confirmBtnsTemplate);

            eventName += id;

            this.confirmEvent.once(eventName, function (confirm) {

                if (!confirm) {
                    return $btnsBlock.html(ctrlBtnsTemplate);
                }

                model.destroy({
                    success: function (model, res, opts) {
                        $todoItem.remove();
                    }
                });

            });

        },

        onEdit: function (e) {
            var self = this;
            var $title;
            var $rating;
            var modelJSON;
            var eventName = 'confirm:';
            var $target = $(e.currentTarget);
            var $todoItem = $target.closest('.todo-item');
            var $infoBlock = $todoItem.find('.info-block');
            var $btnsBlock = $target.closest('.btns-block');
            var id = $todoItem.attr('data-id');
            var model = this.collection.get(id);

            if (!model) {
                return;
            }

            eventName += id;
            modelJSON = model.toJSON();

            $btnsBlock.html(confirmBtnsTemplate);
            $infoBlock.html(this.infoBlockInputTemplate({todo: modelJSON}));

            $title = $infoBlock.find('.title');
            $rating = $infoBlock.find('.rating');

            this.confirmEvent.once(eventName, function (confirm) {
                var title;
                var rating;
                var updateObj;

                $btnsBlock.html(ctrlBtnsTemplate);

                if (!confirm) {
                    return $infoBlock.html(self.infoBlockOutputTemplate({todo: modelJSON}));
                }

                updateObj = {};

                title = $title.val();
                rating = +$rating.val();

                if (title && title !== modelJSON.title) {
                    updateObj.title = title;
                }

                if (rating && rating !== modelJSON.rating) {
                    updateObj.rating = rating;
                }

                if (!Object.keys(updateObj).length) {
                    return $infoBlock.html(self.infoBlockOutputTemplate({todo: modelJSON}));
                }

                model.set(updateObj);

                model.save(null, {
                    patch  : true,
                    success: function (model, res, opts) {
                        $infoBlock.html(self.infoBlockOutputTemplate({todo: model.toJSON()}));
                    },
                    error  : function (model, res, opts) {
                        model.set(modelJSON);
                        $infoBlock.html(self.infoBlockOutputTemplate({todo: modelJSON}));
                        alert('Update error!');
                    }
                });
            });
        },

        onChangeState: function (e) {
            var data;
            var url = 'todos/state/';
            var $target = $(e.currentTarget);
            var id = this.getItemId($target);
            var finished = $target.is(':checked');
            var $title = $target.closest('div').find('.title');
            var model = this.collection.get(id);

            if (!model) {
                return;
            }

            function todoState(finished) {
                if (finished) {
                    $title.addClass('todo-finished');
                } else {
                    $title.removeClass('todo-finished');
                }
            }

            url += id;

            data = {
                finished: finished
            };

            $.ajax(url, {
                method : 'POST',
                data   : data,
                success: function (xhr) {
                    model.set('finished', finished);
                    todoState(finished);
                },
                error  : function (xhr) {
                    finished = !!finished;

                    $target.prop('checked', finished);

                    model.set('finished', finished);
                    todoState(finished);
                }
            });
        },

        onConfirm: function (e) {
            var eventName = 'confirm:';
            var $target = $(e.currentTarget);

            eventName += this.getItemId($target);

            this.confirmEvent.trigger(eventName, true);
        },

        onCancel: function (e) {
            var eventName = 'confirm:';
            var $target = $(e.currentTarget);

            eventName += this.getItemId($target);

            this.confirmEvent.trigger(eventName, false);
        },

        onCreateTodo: function () {
            this.onClearFilter();
            this.renderSubView('create');
        },

        onStateFilter: function () {
            this.renderSubView('state-filter');
        },

        onRatingFilter: function () {
            this.renderSubView('rating-filter');
        },

        onClearFilter: function () {
            this.filter = {};
            this.closeSubView();
            this.fetchCollection();
        },

        closeSubView: function () {
            if (!this.subView) {
                return;
            }

            this.$subView.hide();
            this.subView.close();
            this.subView = null;
        },

        handleAction: function (type, data) {
            switch (type) {
                case CONSTANTS.ACTION_TYPES.CREATE:
                    this.createTodo(data);
                    break;
                case CONSTANTS.ACTION_TYPES.FILTER_STATE:
                    this.stateFilter(data);
                    break;
                case CONSTANTS.ACTION_TYPES.FILTER_SORT:
                    this.sortFilter(data);
                    break;
                case CONSTANTS.ACTION_TYPES.FILTER_RANGE:
                    this.rangeFilter(data);
                    break;
                default:
                    break;
            }
        },

        createTodo: function (data) {
            var self = this;
            var model = new TodoModel(data);

            model.save(null, {
                success: function (mod, res, opts) {
                    self.closeSubView();
                    self.collection.add(mod);
                    self.$list.prepend(self.todoTemplate({todo: res}));
                }
            });
        },

        stateFilter: function (filterType) {
            this.filter.state = filterType;
            this.fetchCollection();
        },

        sortFilter: function (filterSortType) {
            this.filter.sort = filterSortType;
            this.fetchCollection();
        },

        rangeFilter: function (range) {
            this.filter.range = range;
            this.fetchCollection();
        },

        renderSubView: function (name) {
            var self = this;

            require(['views/todos/subviews/' + name + '/view'], function (View) {

                self.closeSubView();
                self.subView = new View();

                self.subView.on(CONSTANTS.EVENTS.ACTION, function (type, data) {
                    self.handleAction(type, data);
                });

                self.$subView.show();

            });

        },

        renderList: function () {
            var data = {
                todoTemplate: this.todoTemplate,
                todos       : this.collection.toJSON()
            };

            this.$list.html(this.todoListTemplate(data));
        },

        render: function () {
            this.$el.html(this.todosTemplate);

            this.$list = this.$el.find('#todos-list');
            this.$subView = this.$el.find('#todos-subview').hide();

            return this;
        }
    });

});
