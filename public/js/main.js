'use strict';

var App = {};

require.config({
    config: {
        waitSeconds: 0
    },

    paths: {
        jQuery    : './libs/jquery/dist/jquery',
        Underscore: './libs/underscore/underscore',
        Backbone  : './libs/backbone/backbone',
        async     : './libs/async/dist/async',
        moment    : './libs/moment/moment',
        text      : './libs/text/text',
        templates : '../templates' // templates dir not error
    },

    shim: {
        jQuery: {
            exports: '$'
        },

        Underscore: {
            exports: '_'
        },

        Backbone : ['Underscore', 'jQuery'],
        Bootstrap: ['jQuery'],
        app      : ['Backbone']
    }
});

require(['jQuery', 'app'], function ($, app) {
    app.initialize();
});
