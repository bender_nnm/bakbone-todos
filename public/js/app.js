define([
    'Underscore',
    'jQuery',
    'Backbone',
    'router'
], function (_, $, Backbone, Router) {
    'use strict';

    var initialize = function () {

        Backbone.View.prototype.close = function () {
            this.$el.html('');
            this.stopListening();
            this.undelegateEvents();
        };

        App.EventBus = _.extend({}, Backbone.Events);

        App.router = new Router();

        Backbone.history.start({
            silent: false
        });
    };

    return {
        initialize: initialize
    };
});