define([
    'underscore',
    'jQuery',
    'Backbone'
], function (_, $, Backbone) {

    'use strict';

    var appRouter;
    appRouter = Backbone.Router.extend({

        modalView : null,
        mainView  : null,
        headerView: null,
        view      : null,

        routes: {
            'home': 'home',
            '*any': 'any'
        },

        initialize: function () {
        },

        loadWrapperView: function (name, params, callback) {
            var self = this;

            require(['views/' + name + '/view'], function (View) {
                if (self.wrapperView) {
                    self.wrapperView.stopListening();
                    self.wrapperView.undelegateEvents();
                }

                self.wrapperView = new View(params);

                if (typeof callback === "function") {
                    callback();
                }
            });
        },

        home: function () {
            this.loadWrapperView('home', null, null);
        },

        any: function () {
            return Backbone.history.navigate('home', {trigger: true});
        }
    });

    return appRouter;
});