'use strict';

define([], function () {

    return {
        EVENTS: {
            ACTION: 'action'
        },

        ACTION_TYPES: {
            CREATE      : 'create',
            FILTER_STATE: 'filter-state',
            FILTER_SORT : 'filter-sort',
            FILTER_RANGE: 'filter-range'
        }
    };
});

