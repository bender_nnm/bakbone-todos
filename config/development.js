process.env.DB_HOST = 'localhost';
process.env.DB_NAME = 'todos_db';
process.env.DB_PORT = 27017;

process.env.HOST = 'localhost:5757';
process.env.WEB_HOST = 'http://localhost:5757';
process.env.PORT = '5757';

process.env.COOKIE_MAX_AGE = 10 * 365 * 24 * 60 * 60 * 1000;
process.env.SESSION_SECRET = '82e9e46262b351b79858455b24aa30256a016350e4a0ab5d725a9bc608d9b';
