module.exports = function (req, res, next) {
    var browser = req.headers['user-agent'];

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    if (/Trident/.test(browser)) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    }

    if (req.method === 'OPTIONS') {
        return res.status(200).send();
    }

    next();
};
