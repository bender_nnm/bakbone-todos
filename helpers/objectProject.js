module.exports = function (object, keys) {
    var project = {};

    if(typeof keys === 'string'){
        keys = [keys];
    }

    if(!(keys instanceof Array)){
        return project;
    }

    keys.forEach(function (key) {
        if(key && (typeof key === 'string' || typeof key === 'number')){
            project[key] = object[key];
        }
    });

    return project;
};
