var ErrorHelper = function () {
    'use strict';

    var DEFAULT_ERROR_NAME = 'Error';
    var DEFAULT_ERROR_MESSAGE = 'error';
    var DEFAULT_ERROR_STATUS = 400;

    function CustomError(options) {
        // http://j-query.blogspot.com/2014/03/custom-error-objects-in-javascript.html
        Error.captureStackTrace(this);

        if (options && options.name) {
            this.name = options.name;
        } else {
            this.name = DEFAULT_ERROR_NAME;
        }

        if (options && options.message) {
            this.message = options.message;
        } else {
            this.message = DEFAULT_ERROR_MESSAGE;
        }

        if (options && options.status) {
            this.status = options.status;
        } else {
            this.status = DEFAULT_ERROR_STATUS;
        }
    }

    CustomError.prototype = Object.create(Error.prototype);

    this.Error = function (opts) {
        opts || (opts = {});

        if (!opts.name) {
            opts.name = 'CustomError';
        }

        if (!opts.message) {
            opts.message = 'Something goes wrong!';
        }

        if (!opts.status) {
            opts.status = 400;
        }

        return new CustomError(opts);
    };
};

module.exports = new ErrorHelper();
