var objectId = require('mongoose').Types.ObjectId;

module.exports = function () {
    Array.prototype.objectID = function () {
        var _arrayOfID = [];

        for (var i = 0, length = this.length; i < length; i++) {

            if (this[i] && typeof this[i] === 'object' && this[i].hasOwnProperty('_id')) {
                _arrayOfID.push(this[i]._id);
            } else {
                if (typeof this[i] === 'string' && this[i].length === 24) {
                    _arrayOfID.push(objectId(this[i]));
                } else {
                    if (this[i] === null || this[i] === 'null') {
                        _arrayOfID.push(null);
                    } else {
                        _arrayOfID.push(this[i]);
                    }
                }
            }

        }

        return _arrayOfID;
    };

    Array.prototype.toStringObjectIds = function () {

        return this.map(function (_objectId) {

            if (_objectId instanceof objectId) {
                return _objectId.toString();
            } else if (typeof _objectId === 'string') {
                return _objectId;
            } else {
                throw new Error({message: 'Incorrect value for ObjectId'});
            }
        });
    };

    Array.prototype.toIntNum = function () {
        var numRegex = /^[0-9]+$/;
        var _arrayOfNum = [];

        for (var i = 0; i < this.length; i++) {
            if (this[i] && typeof this[i] === 'number') {
                _arrayOfNum.push(this[i]);
            } else {
                if (typeof this[i] === 'string' && numRegex.test(this[i])) {
                    _arrayOfNum.push(parseInt(this[i], 10));
                }
            }
        }

        return _arrayOfNum;
    };
};
