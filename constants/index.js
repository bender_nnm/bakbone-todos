'use strict';

module.exports = {
    VALIDATION: {
        PASSWORD    : /.{6,}/,
        EMAIL_REGEXP: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        OBJECT_ID   : /^[0-9a-fA-F]{24}$/
    },
    COLLECTION: {
        TODOS: 'todos'
    },
    MODELS    : {
        TODO: 'Todo'
    }
};

/*ADMIN_EMAIL: 'admin@admin.com',
 ADMIN_PASSWORD: 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', //12345678
 ROLES: {
 SUPER_ADMIN: 1,
 ADMIN: 2,
 CART_USER: 10,
 VR_USER: 11
 },
 MODELS: {
 USER: 'User'
 },*/
