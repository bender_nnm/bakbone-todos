module.exports = function (app) {
    'use strict';

    var todosRouter = require('./todos')(app);

    app.use('/todos', todosRouter);
};