var express = require('express');
var router = express.Router();
var TodosHandler = require('../handlers/todos');

module.exports = function (app) {
    'use strict';

    var todosHandler = new TodosHandler(app);

    router.get('/', todosHandler.fetch);
    router.post('/', todosHandler.create);
    router.get('/:id', todosHandler.fetchById);
    router.post('/state/:id', todosHandler.patchState);
    router.patch('/:id', todosHandler.patch);
    router.delete('/:id', todosHandler.delete);

    return router;
};
