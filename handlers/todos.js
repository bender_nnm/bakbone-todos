var async = require('async');
var mongoose = require('mongoose');
var _ = require('underscore');

var project = require('../helpers/objectProject');
var CONSTANTS = require('../constants/index');
var errorHelper = require('../helpers/errorHelper');

var TodosHandler = function (app) {

    var db = app.get('db');
    var Todo = db.model(CONSTANTS.MODELS.TODO);

    function validateRating(rating) {
        rating = parseInt(rating, 10);

        if (!rating || rating < 0 || rating > 5) {
            return null;
        }

        return rating;
    }

    this.fetch = function (req, res, next) {
        var sortPipeline;
        var filter = req.query.filter || {};
        var sortFilter = filter.sort;
        var stateFilter = filter.state;
        var rangeFilter = filter.range || {};
        var rangeFilterLte = rangeFilter.lte;
        var rangeFilterGte = rangeFilter.gte;
        var aggregateParams = [];

        if (stateFilter && stateFilter !== 'all') {
            stateFilter = stateFilter !== 'active';

            aggregateParams.push({
                $match: {
                    finished: stateFilter
                }
            });
        }

        if (sortFilter) {
            sortFilter = sortFilter === 'top' ? -1 : 1;

            sortPipeline = {
                rating: sortFilter
            };
        } else {
            sortPipeline = {
                createdAt: -1
            };
        }

        if (rangeFilter && rangeFilterLte && rangeFilterGte) {

            rangeFilterLte = parseInt(rangeFilterLte, 10);
            rangeFilterGte = parseInt(rangeFilterGte, 10);

            rangeFilterLte = rangeFilterLte || 5;
            rangeFilterGte = rangeFilterGte || 1;

            if(rangeFilter.gte <= rangeFilter.lte){
                aggregateParams.push({
                    $match: {
                        rating: {
                            $lte: rangeFilterLte,
                            $gte: rangeFilterGte
                        }
                    }
                });
            }

        }

        aggregateParams.push({
            $sort: sortPipeline
        });

        aggregateParams.push({
            $project: {
                _id     : 1,
                title   : 1,
                rating  : 1,
                finished: 1
            }
        });

        Todo.aggregate(aggregateParams, function (err, todos) {
            if (err) {
                return next(err);
            }

            res.status(200).send(todos);
        });
    };

    this.patchState = function (req, res, next) {
        var body = req.body;
        var id = req.params.id;
        var finished = body.finished === 'true';
        var projectConfig = ['finished'];

        if (!CONSTANTS.VALIDATION.OBJECT_ID.test(id)) {
            return next(errorHelper.Error({message: 'Invalid object id.'}));
        }

        async.waterfall([

            //get todoModel
            function (cb) {
                Todo.findById(id, function (err, todoModel) {
                    if (err) {
                        return cb(err);
                    }

                    if (!todoModel) {
                        return cb(errorHelper.Error({
                            name   : 'NotFound.',
                            message: 'Todo model didn\'t find.',
                            status : 404
                        }));
                    }

                    cb(null, todoModel);
                });
            },

            //set update fields
            function (todoModel, cb) {
                if (finished === todoModel.get('finished')) {
                    return cb(null, project(todoModel.toJSON(), projectConfig));
                }

                todoModel.set('finished', finished);
                todoModel.set('updateDate', new Date());

                todoModel.save(function (err, todoModel) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null, project(todoModel.toJSON(), projectConfig));
                });
            }

        ], function (err, result) {
            if (err) {
                return next(err);
            }

            res.status(200).send(result);
        });
    };

    this.create = function (req, res, next) {
        var todoOpts;
        var todoModel;
        var body = req.body;
        var title = body.title.trim();
        var rating = validateRating(body.rating);
        var projectConfig = ['_id', 'title', 'rating', 'finished'];

        if (!title) {
            return next(errorHelper.Error({message: 'Title is required!'}));
        }

        todoOpts = {title: title};

        if (rating) {
            todoOpts.rating = rating;
        }

        todoModel = new Todo(todoOpts);

        todoModel.save(function (err, todoModel) {
            var todo;

            if (err) {
                return next(err);
            }

            todo = project(todoModel.toJSON(), projectConfig);

            res.status(201).send(todo);
        });
    };

    this.fetchById = function (req, res, next) {
        var id = req.params.id;
        var project = {
            _id     : 1,
            title   : 1,
            rating  : 1,
            finished: 1
        };

        if (!CONSTANTS.VALIDATION.OBJECT_ID.test(id)) {
            return next(errorHelper.Error({message: 'Invalid object id.'}));
        }

        Todo.findById(id, project, function (err, todoModel) {
            if (err) {
                return next(err);
            }

            if (!todoModel) {
                return next(errorHelper.Error({name: 'NotFound.', message: 'Todo model didn\'t find.', status: 404}));
            }

            res.status(200).send(todoModel.json());
        });
    };

    this.patch = function (req, res, next) {
        var body = req.body;
        var id = req.params.id;
        var title = body.title;
        var rating = validateRating(body.rating);
        var projectConfig = ['_id', 'title', 'rating', 'finished'];

        if (!CONSTANTS.VALIDATION.OBJECT_ID.test(id)) {
            return next(errorHelper.Error({message: 'Invalid object id.'}));
        }

        async.waterfall([

            //get todoModel
            function (cb) {
                Todo.findById(id, function (err, todoModel) {
                    if (err) {
                        return cb(err);
                    }

                    if (!todoModel) {
                        return cb(errorHelper.Error({
                            name   : 'NotFound.',
                            message: 'Todo model didn\'t find.',
                            status : 404
                        }));
                    }

                    cb(null, todoModel);
                });
            },

            //set update fields
            function (todoModel, cb) {
                if (title && title !== todoModel.get('title')) {
                    todoModel.set('title', title);
                }

                if (rating && rating !== todoModel.get('rating')) {
                    todoModel.set('rating', rating);
                }

                todoModel.set('updateDate', new Date());

                todoModel.save(function (err, todoModel) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null, project(todoModel.toJSON(), projectConfig));
                });
            }

        ], function (err, result) {
            if (err) {
                return next(err);
            }

            res.status(200).send(result);
        });
    };

    this.delete = function (req, res, next) {
        var id = req.params.id;

        if (!CONSTANTS.VALIDATION.OBJECT_ID.test(id)) {
            return next(errorHelper.Error({message: 'Invalid object id.'}));
        }

        async.waterfall([

            function (cb) {
                Todo.findById(id, function (err, todoModel) {
                    if (err) {
                        return cb(err);
                    }

                    if (!todoModel) {
                        return next(errorHelper.Error({
                            name   : 'NotFound.',
                            message: 'Todo model didn\'t find.',
                            status : 404
                        }));
                    }

                    cb(null);
                });
            },

            function (cb) {
                Todo.remove({_id: id}, function (err) {
                    if (err) {
                        return next(err);
                    }

                    cb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({'OK': 1});
        });
    };

};

module.exports = TodosHandler;