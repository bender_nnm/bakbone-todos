var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CONSTANTS = require('../constants');

module.exports = function (db) {
    var schema = new Schema({
        title: {
            type    : String,
            required: true
        },

        rating: {
            type: Number
        },

        finished: {
            type   : Boolean,
            default: false
        },

        createdAt: {
            type   : Date,
            default: Date.now
        },

        updateAt: {
            type   : Date,
            default: Date.now
        }
    }, {
        collection: CONSTANTS.COLLECTION.TODOS
    });

    db.model(CONSTANTS.MODELS.TODO, schema);
};